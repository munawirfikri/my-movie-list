package com.munawirfikri.mymovielist.data.remote.response

import com.google.gson.annotations.SerializedName

data class MovieResponse (

        @field:SerializedName("results")
        val results: List<MovieItem>
        )

data class MovieItem (
        @field:SerializedName("id")
        val id: String? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("backdrop_path")
        val poster: String? = null,

        @field:SerializedName("overview")
        val synopsis: String? = null,

        @field:SerializedName("release_date")
        val releaseDate: String? = null,

        @field:SerializedName("status")
        val status: String? = null,

        @field:SerializedName("genres")
        val genres: List<GenreItem>
        )