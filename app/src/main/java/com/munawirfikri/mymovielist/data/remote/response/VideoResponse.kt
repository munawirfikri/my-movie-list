package com.munawirfikri.mymovielist.data.remote.response

import com.google.gson.annotations.SerializedName

data class VideoResponse (
    @field:SerializedName("results")
    val results: List<VideoItem>
    )

data class VideoItem (
    @field:SerializedName("type")
    val type: String,

    @field:SerializedName("key")
    val key: String
        )