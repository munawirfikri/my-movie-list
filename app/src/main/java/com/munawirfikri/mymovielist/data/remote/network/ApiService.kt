package com.munawirfikri.mymovielist.data.remote.network

import com.munawirfikri.mymovielist.data.remote.response.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("3/genre/movie/list")
    fun getMovieGenres(@Query("api_key") apiKey: String): Call<GenreResponse>

    @GET("3/discover/movie")
    fun getMovies(
        @Query("api_key")
        apiKey: String,
        @Query("with_genres")
        withGenres: String
    ): Call<MovieResponse>

    @GET("3/movie/{id}")
    fun getDetailMovies(
        @Path("id") id: String,
        @Query("api_key") apiKey: String
    ): Call<MovieItem>

    @GET("3/movie/{id}/reviews")
    fun getMovieReviews(
        @Path("id") id: String,
        @Query("api_key") apiKey: String
    ): Call<ReviewResponse>

    @GET("3/movie/{id}/videos")
    fun getMovieTrailer(
        @Path("id") id: String,
        @Query("api_key") apiKey: String
    ): Call<VideoResponse>
}