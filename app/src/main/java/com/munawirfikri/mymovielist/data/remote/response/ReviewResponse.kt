package com.munawirfikri.mymovielist.data.remote.response

import com.google.gson.annotations.SerializedName

data class ReviewResponse (
    @field:SerializedName("results")
    val results: List<ReviewItem> )

data class ReviewItem(

    @field:SerializedName("author_details")
    val authorDetails: AuthorDetailItem,

    @field:SerializedName("content")
    val content: String

)

data class AuthorDetailItem (
    @field:SerializedName("username")
    val username: String,

    @field:SerializedName("avatar_path")
    val avatarPath: String,

    @field:SerializedName("rating")
    val rating: String
        )