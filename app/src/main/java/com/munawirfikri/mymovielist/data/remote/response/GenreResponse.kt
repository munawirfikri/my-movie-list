package com.munawirfikri.mymovielist.data.remote.response

import com.google.gson.annotations.SerializedName

data class GenreResponse (
    @field:SerializedName("genres")
    val genres: List<GenreItem>
)

data class GenreItem (
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("name")
    val name: String? = null
)