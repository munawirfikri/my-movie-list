package com.munawirfikri.mymovielist.ui.movie

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.munawirfikri.mymovielist.R
import com.munawirfikri.mymovielist.databinding.ActivityMovieBinding

class MovieActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMovieBinding
    private val movieViewModel: MovieViewModel by viewModels()
    private var movieAdapter: MovieAdapter? = null

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_NAME = ""
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        movieAdapter = MovieAdapter()
        val extras = intent.extras

        if(extras!= null){
            val extraID = extras.getString(EXTRA_ID)
            if(extraID!=null){
                movieViewModel.showMovies(extraID)
                movieViewModel.movies.observe(this){
                        movieAdapter?.setData(it)
                    movieAdapter?.notifyDataSetChanged()
                }
            }
            val extraName = extras.getString(EXTRA_NAME)

            if(extraName!=""){
                binding.tvGenre.text = EXTRA_NAME
            }
        }

        movieViewModel.isLoading.observe(this){
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }

        binding.ivBack.setOnClickListener(this)



            with(binding.rvMovie){
            this.layoutManager = LinearLayoutManager(context)
            this.setHasFixedSize(true)
            this.adapter = movieAdapter
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.iv_back -> finish()
        }
    }
}