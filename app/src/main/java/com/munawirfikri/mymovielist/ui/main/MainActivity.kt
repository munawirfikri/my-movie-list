package com.munawirfikri.mymovielist.ui.main

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.munawirfikri.mymovielist.R
import com.munawirfikri.mymovielist.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    private var genreAdapter: GenreAdapter? = null

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)

        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        genreAdapter = GenreAdapter()
        mainViewModel.showGenres()
        mainViewModel.genre.observe(this) {
            genreAdapter?.setData(it)
            genreAdapter?.notifyDataSetChanged()
        }

        mainViewModel.isLoading.observe(this){
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }

        binding.ivInfo.setOnClickListener(this)

        with(binding.rvGenre){
            this.layoutManager = GridLayoutManager(context, 2)
            this.adapter = genreAdapter
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.iv_info -> Snackbar.make(binding.root, "Created by Munawir Fikri", Snackbar.LENGTH_SHORT).show()
        }
    }
}