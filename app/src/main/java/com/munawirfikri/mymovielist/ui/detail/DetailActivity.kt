package com.munawirfikri.mymovielist.ui.detail

import android.annotation.SuppressLint
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.munawirfikri.mymovielist.R
import com.munawirfikri.mymovielist.databinding.ActivityDetailBinding
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.DefaultPlayerUiController

class DetailActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityDetailBinding
    private val detailViewModel: DetailViewModel by viewModels()
    private var reviewAdapter: ReviewAdapter? = null


    companion object {
        const val EXTRA_ID = "extra_id"

    }
    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)

        reviewAdapter = ReviewAdapter()

        val extras = intent.extras

        if(extras!=null){
            val extraID = extras.getString(EXTRA_ID)

            if(extraID!=null){
                detailViewModel.getDetailMovie(extraID.toString())
                detailViewModel.movie.observe(this){
                    var genre = ""
                    for(item in it.genres){
                        genre += "${item.name}, "
                    }
                    val genres = genre.dropLast(2)

                    binding.tvGenres.text = "Genres: $genres"
                    binding.tvStatus.text = "Status: ${it.status.toString()}"
                    binding.tvTitle.text = it.title.toString()
                    binding.tvDateRelease.text = "Release Date: ${it.releaseDate.toString()}"
                    binding.tvSynopsis.text = "Synopsis: ${it.synopsis.toString()}"
                }

                detailViewModel.getReviews(extraID.toString())
                detailViewModel.reviews.observe(this){
                    if(it.size < 1) {
                        binding.tvTitleReview.text = "No reviews yet"
                    }
                    reviewAdapter?.setData(it)
                    reviewAdapter?.notifyDataSetChanged()
                }
                detailViewModel.getVideos(extraID.toString())

                detailViewModel.videos.observe(this){
                    if(it.size > 0){
                        getVideo(it[it.lastIndex].key)
                    }else{
                        binding.youtubePlayerViewLayout.visibility = View.GONE
                    }
                }
            }
        }

        binding.ivBack.setOnClickListener(this)

        with(binding.rvReview){
            this.layoutManager = LinearLayoutManager(context)
            this.setHasFixedSize(true)
            this.adapter = reviewAdapter
        }

        setContentView(binding.root)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.iv_back -> finish()
        }
    }

    fun getVideo(videoId: String){
        val thirdPartyYoutubePlayerView = binding.thirdPartyPlayerView
        thirdPartyYoutubePlayerView.enableAutomaticInitialization = false

        val listener: YouTubePlayerListener = object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                // We're using pre-made custom ui
                val defaultPlayerUiController =
                    DefaultPlayerUiController(thirdPartyYoutubePlayerView, youTubePlayer)
                defaultPlayerUiController.showFullscreenButton(true)

                // When the video is in full-screen, cover the entire screen
                defaultPlayerUiController.setFullScreenButtonClickListener {
                    if (thirdPartyYoutubePlayerView.isFullScreen()) {
                        thirdPartyYoutubePlayerView.exitFullScreen()
                        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
                        // Show ActionBar
                        if (supportActionBar != null) {
                            supportActionBar!!.show()
                        }
                    } else {
                        thirdPartyYoutubePlayerView.enterFullScreen()
                        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
                        // Hide ActionBar
                        if (supportActionBar != null) {
                            supportActionBar!!.hide()
                        }
                    }
                }


                thirdPartyYoutubePlayerView.setCustomPlayerUi(defaultPlayerUiController.rootView)

                youTubePlayer.cueVideo(videoId, 0f)
            }
        }

        // Disable iFrame UI
        val options: IFramePlayerOptions = IFramePlayerOptions.Builder().controls(0).build()
        thirdPartyYoutubePlayerView.initialize(listener, options)
    }

}