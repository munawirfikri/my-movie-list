package com.munawirfikri.mymovielist.ui.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.munawirfikri.mymovielist.BuildConfig
import com.munawirfikri.mymovielist.data.remote.network.ApiConfig
import com.munawirfikri.mymovielist.data.remote.response.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel: ViewModel() {

    private val _movie = MutableLiveData<MovieItem>()
    val movie : LiveData<MovieItem> = _movie

    private val _reviews = MutableLiveData<List<ReviewItem>>()
    val reviews: LiveData<List<ReviewItem>> = _reviews

    private val _videos = MutableLiveData<List<VideoItem>>()
    val videos: LiveData<List<VideoItem>> = _videos

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val apiKey = BuildConfig.TMDB_KEY

    companion object{
        private const val TAG = "DetailViewModel"
    }


    fun getDetailMovie(id: String){
        _isLoading.value = true

        val client = ApiConfig.provideTMDBApiService().getDetailMovies(id, apiKey)
        client.enqueue(object: Callback<MovieItem> {
            override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _movie.value = response.body()
                }else{
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                _isLoading.value = false
                Log.e(TAG, "onFailure: ${t.message.toString()}")
            }

        })
    }

    fun getReviews(id: String){
        _isLoading.value = true

        val client = ApiConfig.provideTMDBApiService().getMovieReviews(id, apiKey)
        client.enqueue(object : Callback<ReviewResponse> {
            override fun onResponse(
                call: Call<ReviewResponse>,
                response: Response<ReviewResponse>
            ) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _reviews.value = response.body()?.results
                }else{
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<ReviewResponse>, t: Throwable) {
                _isLoading.value = false
                Log.e(TAG, "onFailure: ${t.message.toString()}")
            }

        })
    }

    fun getVideos(id: String){
        _isLoading.value = true

        val client = ApiConfig.provideTMDBApiService().getMovieTrailer(id, apiKey)
        client.enqueue(object : Callback<VideoResponse> {
            override fun onResponse(call: Call<VideoResponse>, response: Response<VideoResponse>) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _videos.value = response.body()?.results
                }else{
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<VideoResponse>, t: Throwable) {
                _isLoading.value = false
                Log.e(TAG, "onFailure: ${t.message.toString()}")
            }

        })
    }
}