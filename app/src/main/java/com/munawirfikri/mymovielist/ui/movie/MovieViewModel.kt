package com.munawirfikri.mymovielist.ui.movie

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.munawirfikri.mymovielist.BuildConfig
import com.munawirfikri.mymovielist.data.remote.network.ApiConfig
import com.munawirfikri.mymovielist.data.remote.response.MovieItem
import com.munawirfikri.mymovielist.data.remote.response.MovieResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieViewModel: ViewModel() {

    private val _movies = MutableLiveData<List<MovieItem>>()
    val movies: LiveData<List<MovieItem>> = _movies

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val apiKey = BuildConfig.TMDB_KEY

    companion object{
        private const val TAG = "MovieViewModel"
    }


    fun showMovies(genre: String){
        _isLoading.value = true

        val client = ApiConfig.provideTMDBApiService().getMovies(apiKey, genre)
        client.enqueue(object : Callback<MovieResponse>{
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _movies.value = response.body()?.results
                }else{
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                _isLoading.value = false
                Log.e(TAG, "onFailure: ${t.message.toString()}")
            }

        })
    }
}