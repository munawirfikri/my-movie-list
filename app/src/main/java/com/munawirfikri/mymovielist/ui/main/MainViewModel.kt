package com.munawirfikri.mymovielist.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.munawirfikri.mymovielist.BuildConfig
import com.munawirfikri.mymovielist.data.remote.network.ApiConfig
import com.munawirfikri.mymovielist.data.remote.response.GenreItem
import com.munawirfikri.mymovielist.data.remote.response.GenreResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel: ViewModel() {

    private val _genre = MutableLiveData<List<GenreItem>>()
    val genre: LiveData<List<GenreItem>> = _genre

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val apiKey = BuildConfig.TMDB_KEY

    companion object{
        private const val TAG = "MainViewModel"
    }


    fun showGenres(){
        _isLoading.value = true

        val client = ApiConfig.provideTMDBApiService().getMovieGenres(apiKey)
        client.enqueue(object : Callback<GenreResponse> {
            override fun onResponse(call: Call<GenreResponse>, response: Response<GenreResponse>) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _genre.value = response.body()?.genres
                }else{
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<GenreResponse>, t: Throwable) {
                _isLoading.value = false
                Log.e(TAG, "onFailure: ${t.message.toString()}")
            }

        })
    }
}