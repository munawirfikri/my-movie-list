package com.munawirfikri.mymovielist.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.munawirfikri.mymovielist.R
import com.munawirfikri.mymovielist.data.remote.response.ReviewItem
import com.munawirfikri.mymovielist.databinding.ItemReviewBinding

class ReviewAdapter: RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder>() {

    private var listData = ArrayList<ReviewItem>()

    fun setData(newListData: List<ReviewItem>){
        if(newListData == null) return
        listData.clear()
        listData.addAll(newListData)
        notifyDataSetChanged()
    }


    class ReviewViewHolder(private val binding: ItemReviewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(reviewItem: ReviewItem){
            with(binding){
                tvUsername.text = reviewItem.authorDetails.username
                tvReview.text = reviewItem.content
                if(reviewItem.authorDetails.rating != null){
                    tvRating.text = "Rating: ${reviewItem.authorDetails.rating}/10"
                }else{
                    tvRating.text = "Rating: -"
                }
                Glide.with(itemView.context)
                    .load("https://www.themoviedb.org/t/p/w220_and_h330_face" + reviewItem.authorDetails.avatarPath)
                    .centerCrop().apply(
                        RequestOptions.placeholderOf(R.drawable.ic_loading)
                            .error(R.drawable.ic_broken_image))
                    .into(ivAvatar)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val itemReviewBinding = ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewViewHolder(itemReviewBinding)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        val data = listData[position]
        holder.bind(data)
    }

    override fun getItemCount() = listData.size

}