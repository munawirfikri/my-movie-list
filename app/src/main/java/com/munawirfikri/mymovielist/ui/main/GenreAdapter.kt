package com.munawirfikri.mymovielist.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.munawirfikri.mymovielist.data.remote.response.GenreItem
import com.munawirfikri.mymovielist.databinding.ItemGenreBinding
import com.munawirfikri.mymovielist.ui.movie.MovieActivity

class GenreAdapter: RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {

    private var listData = ArrayList<GenreItem>()

    fun setData(newListData: List<GenreItem>){
        if(newListData == null) return
        listData.clear()
        listData.addAll(newListData)
        notifyDataSetChanged()
    }


    class GenreViewHolder(private val binding: ItemGenreBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(genreItem: GenreItem){
            with(binding){
                tvGenre.text = genreItem.name
            }
            itemView.setOnClickListener{
                val intent = Intent(itemView.context, MovieActivity::class.java)
                intent.putExtra(MovieActivity.EXTRA_ID, genreItem.id.toString())
                intent.putExtra(MovieActivity.EXTRA_NAME, genreItem.name.toString())
                itemView.context.startActivity(intent)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val itemGenreBinding = ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GenreViewHolder(itemGenreBinding)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        val data = listData[position]
        holder.bind(data)
    }

    override fun getItemCount() = listData.size

}